## 0.6.2 (2024-12-26)

### Fix

- fix partial fix multi-arch tagging, still broken if using a persistent tag.

## 0.6.1 (2024-12-26)

### Fix

- handle non-exsistent image

## 0.6.0 (2024-12-26)

### BREAKING CHANGE

- 
inputs.runner_tag should now use sass-linux-small
inputs.arch added as the last arg of runner tag, and images will be tagged in the registry as $TAG-$ARCH, and added to a shared manifest of $IMAGE:$TAG

### Feat

- handle multi-arch container builds

## 0.5.1 (2024-12-26)

### Fix

- per runner_tag job names and test multi-arch build job

## 0.5.0 (2024-12-25)

### Feat

- use buildx to support multi-arch builds

### Fix

- add when input to build component

## 0.4.6 (2024-12-05)

### Fix

- inputs reference in build component

## 0.4.5 (2024-12-05)

### Fix

- add runner_tag input to the build component

## 0.4.4 (2024-11-05)

### Fix

- scratch not scrach

## 0.4.3 (2024-11-05)

### Fix

- update build rules

## 0.4.2 (2024-11-05)

### Fix

- fix multi stage build

## 0.4.1 (2024-11-05)

### Fix

- try including a cached image build tag

## 0.4.0 (2024-11-05)

### Feat

- only push tags and add use_cached_build

### Fix

- use string for use_cached_build

## 0.3.2 (2024-04-27)

### Fix

- nevermind on v prefix

## 0.3.1 (2024-04-27)

### Fix

- trigger release with v prefix

## 0.3.0 (2024-04-27)

## 0.3.0-rc2 (2024-04-27)

## 0.3.0-rc1 (2024-04-27)

## 0.3.0-rc0 (2024-04-27)

### Feat

- add rc tagging

## 0.2.0 (2024-04-25)

### Feat

- move to gitops-tools group

## 0.1.15 (2024-04-25)

### Fix

- rename target_repo

## 0.1.14 (2024-04-25)

### Fix

- update downstream infrastructure url and target_repo

## 0.1.13 (2024-04-25)

### Fix

- ignore triggered job

## 0.1.12 (2024-04-25)

### Fix

- jq syntax

## 0.1.11 (2024-04-25)

### Fix

- split out job added script for debugging

## 0.1.10 (2024-04-24)

### Fix

- update deploy

## 0.1.9 (2024-04-24)

### Fix

- bash syntax again

## 0.1.8 (2024-04-24)

### Fix

- cleanup expected job outputs

## 0.1.7 (2024-04-24)

### Fix

- update bash syntax

## 0.1.6 (2024-04-24)

### Fix

- update conditional for ensure-job-added

## 0.1.5 (2024-04-24)

### Fix

- standardize input naming conventions feat: add test, licence and infrastructure_repo input

## 0.1.4 (2024-04-23)

### Fix

- build off tags by default to skip duplicate image builds

## 0.1.3 (2024-04-23)

### Fix

- stop duplicate builds and don't fail bump on no version change

## 0.1.2 (2024-04-23)

### Fix

- make build/bump/deploy interruptable

## 0.1.1 (2024-04-23)

### Fix

- add Containerfile for full testing and update commit tag regex

## 0.1.0 (2024-04-23)

### Feat

- add build/bump/deploy/release templates

### Fix

- update image-name reference
